﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp23
{
    class Program
    {
        static void Main(string[] args)
        {
            var h = new Õpilane() { Nimi = "Henn", Klass = 3 };
            var j = new Õpilane() { Nimi = "Jaan", Klass = 2 };
            var p = new Õpilane() { Nimi = "Peeter", Klass = 3 };

            Klass k = new Klass() { number = 3, Nimi = "3B" };

            foreach(var x in k.Õpilased)
                Console.WriteLine(x.Nimi);

            h.Klass = 4;

            foreach (var x in k.Õpilased)
                Console.WriteLine(x.Nimi);


        }
    }

    class Õpilane
    {
        public static List<Õpilane> Õpilased = new List<Õpilane>();
        public string Nimi;
        public int Klass;

        public Õpilane() { Õpilased.Add(this); }
    }

    class Klass
    {
        public int number;
        public string Nimi;

        public IEnumerable<Õpilane> Õpilased
        {
            get => Õpilane.Õpilased.Where(x => x.Klass == this.number);
        }
    }

}
